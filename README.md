# objpath

Small library on top of reflect to get values by object paths in Structs or Maps.
Basic aggregation is implemented.

## Installation

```
go get gitlab.com/mgutz/objpath
```

## Example

```go
type Cast struct {
  Actor, Role string
}

type Movies struct {
  Cast []Cast
}

movies := map[string]Movies{
  "Dr. No": {Cast: []Cast{
    {Actor: "Sean Connery", Role: "Bond"},
    {Actor: "Joseph Wiseman", Role: "Dr. No"},
  }},
}

q := `["Dr. No"].Cast[0].Actor`
value, _ := Get(movies, q)
fmt.Println(q, "->", value.Interface())   // ["Dr. No"].Cast[0].Actor -> Sean Connery

// aggregate Cast slice values by omitting index
q = `Dr\. No.Cast.Role`
value, _ = Get(movies, q)
fmt.Println(q, "->", value.Interface())   // Dr\. No.Cast.Role -> [Bond Dr. No]

q = `Dr\. No.Cast[0].Actor`
value, _ = Get(movies, q)
fmt.Println(q, "->", value.Interface())   // Dr\. No.Cast[0].Actor -> Sean Connery
```

### Case-insensitive matching

Use the `LookupI` and `GetI` functions to do a case-insensitive match on struct field names and map keys. It will first look for an exact match; if that fails, it will fall back to a more expensive linear search over fields/keys.

## Credit

Forked of (and heavily modified) [github.com/mcuadros/go-lookup](https://github.com/mcuadros/go-lookup).

Motivation:

- "." periods could not be used within keys, nor could indices use string values
- could not step debug individual tests in VS Code

## License

MIT, see [LICENSE](LICENSE)
