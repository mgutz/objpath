package objpath

import (
	"fmt"
	"reflect"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestLookup_Map(t *testing.T) {
	is := require.New(t)
	value, err := Lookup(map[string]int{"foo": 42}, "foo")
	is.NoError(err)
	is.Equal(value.Int(), int64(42))
}

func TestLookup_Ptr(t *testing.T) {
	is := require.New(t)
	value, err := Lookup(&structFixture, "String")
	is.NoError(err)

	is.Equal(value.String(), "foo")
}

func TestLookup_Interface(t *testing.T) {
	is := require.New(t)

	value, err := Lookup(structFixture, "Interface")

	is.NoError(err)
	is.Equal(value.String(), "foo")
}

func TestLookup_StructBasic(t *testing.T) {
	is := require.New(t)
	value, err := Lookup(structFixture, "String")
	is.NoError(err)
	is.Equal(value.String(), "foo")
}

func TestLookup_StructPlusMap(t *testing.T) {
	is := require.New(t)
	value, err := Lookup(structFixture, "Map", "foo")
	is.NoError(err)
	is.Equal(value.Int(), int64(42))
}

func TestLookup_MapNamed(t *testing.T) {
	is := require.New(t)
	value, err := Lookup(mapFixtureNamed, "foo")
	is.NoError(err)
	is.Equal(value.Int(), int64(42))
}

func TestLookup_NotFound(t *testing.T) {
	is := require.New(t)

	_, err := Lookup(structFixture, "qux")
	is.Equal(err, ErrKeyNotFound)

	_, err = Lookup(mapFixture, "qux")
	is.Equal(err, ErrKeyNotFound)
}

func TestAggregableLookup_StructIndex(t *testing.T) {
	is := require.New(t)
	value, err := Lookup(structFixture, "StructSlice", "Map", "foo")

	is.NoError(err)
	is.Equal(value.Interface(), []int{42, 42})
}

func TestAggregableLookup_StructNestedMap(t *testing.T) {
	is := require.New(t)
	value, err := Lookup(structFixture, "StructSlice", 0, "String")

	is.NoError(err)
	is.Equal(value.Interface(), "foo")
}

func TestAggregableLookup_StructNested(t *testing.T) {
	is := require.New(t)
	value, err := Lookup(structFixture, "StructSlice", "StructSlice", "String")

	is.NoError(err)
	is.Equal(value.Interface(), []string{"bar", "foo", "qux", "baz"})
}

func TestLookup_Map_Period(t *testing.T) {
	is := require.New(t)
	value, err := Get(mapComplexFixture, `nested.fav\.color`)
	is.NoError(err)
	is.Equal(value.String(), "blue")
}

func TestLookup_Slice(t *testing.T) {
	is := require.New(t)
	value, err := Get(intSliceFixture, `[2][0]`)
	is.NoError(err)
	is.Equal(value.Interface(), 100)
}

func TestAggregableLookupString_Complex(t *testing.T) {
	is := require.New(t)

	value, err := Get(structFixture, "StructSlice.StructSlice[0].String")
	is.NoError(err)
	is.Equal(value.Interface(), []string{"bar", "qux"})

	value, err = Get(structFixture, "StructSlice.StructSlice.String")
	is.NoError(err)
	is.Equal(value.Interface(), []string{"bar", "foo", "qux", "baz"})

	value, err = Get(structFixture, "StructSlice[0].Map.foo")
	is.NoError(err)
	is.Equal(value.Interface(), 42)

	value, err = Get(mapComplexFixture, "map.bar")
	is.NoError(err)
	is.Equal(value.Interface(), 1)

	value, err = Get(mapComplexFixture, "list.baz")
	is.NoError(err)
	is.Equal(value.Interface(), []int{1, 2, 3})
}

func TestAggregableLookup_EmptySlice(t *testing.T) {
	is := require.New(t)

	fixture := [][]MyStruct{{}}
	_, err := Get(fixture, "String")
	is.Equal(err, ErrKeyNotFound)
	// is.NoError(err)
	// is.Equal(value.Interface().([]string), []string{})
}

func TestAggregableLookup_EmptyMap(t *testing.T) {
	is := require.New(t)
	fixture := map[string]*MyStruct{}
	_, err := Get(fixture, "Map")
	is.Equal(err, ErrKeyNotFound)
	// is.NoError(err)
	// is.Equal(value.Interface().([]map[string]int), []map[string]int{})
}

func TestMergeValue(t *testing.T) {
	is := require.New(t)
	v := mergeValue([]reflect.Value{reflect.ValueOf("qux"), reflect.ValueOf("foo")})
	is.Equal(v.Interface(), []string{"qux", "foo"})
}

func TestMergeValueSlice(t *testing.T) {
	is := require.New(t)

	v := mergeValue([]reflect.Value{
		reflect.ValueOf([]string{"foo", "bar"}),
		reflect.ValueOf([]string{"qux", "baz"}),
	})

	is.Equal(v.Interface(), []string{"foo", "bar", "qux", "baz"})
}

func TestMergeValueZero(t *testing.T) {
	is := require.New(t)
	v := mergeValue([]reflect.Value{reflect.Value{}, reflect.ValueOf("foo")})
	is.Equal(v.Interface(), []string{"foo"})
}

func TestLookup_CaseSensitive(t *testing.T) {
	is := require.New(t)
	_, err := Lookup(structFixture, "STring")
	is.Equal(err, ErrKeyNotFound)
}

func TestLookup_CaseInsensitive(t *testing.T) {
	is := require.New(t)
	value, err := LookupI(structFixture, "STring")
	is.NoError(err)
	is.Equal(value.String(), "foo")
}

func TestLookup_CaseInsensitive_ExactMatch(t *testing.T) {
	is := require.New(t)
	value, err := LookupI(caseFixtureStruct, "Testfield")
	is.NoError(err)
	is.Equal(value.Int(), int64(2))
}

func TestLookup_CaseInsensitive_FirstMatch(t *testing.T) {
	is := require.New(t)
	value, err := LookupI(caseFixtureStruct, "testfield")
	is.NoError(err)
	is.Equal(value.Int(), int64(1))
}

func TestLookup_CaseInsensitiveExactMatch(t *testing.T) {
	is := require.New(t)
	value, err := LookupI(structFixture, "STring")
	is.NoError(err)
	is.Equal(value.String(), "foo")
}

func TestLookup_Map_CaseSensitive(t *testing.T) {
	is := require.New(t)
	_, err := Lookup(map[string]int{"Foo": 42}, "foo")
	is.Equal(err, ErrKeyNotFound)
}

func TestLookup_Map_CaseInsensitive(t *testing.T) {
	is := require.New(t)
	value, err := LookupI(map[string]int{"Foo": 42}, "foo")
	is.NoError(err)
	is.Equal(value.Int(), int64(42))
}

func TestLookup_Map_CaseInsensitive_ExactMatch(t *testing.T) {
	is := require.New(t)
	value, err := LookupI(caseFixtureMap, "Testkey")
	is.NoError(err)
	is.Equal(value.Int(), int64(2))
}

// Go does not guarantee order so first match can differ
//
// func (s *S) TestLookup_Map_CaseInsensitive_FirstMatch(c *C) {
// 	value, err := LookupI(caseFixtureMap, "testkey")
// 	c.Assert(err, IsNil)
// 	c.Assert(value.Int(), Equals, int64(1))
// }

func TestLookup_ListPtr(t *testing.T) {
	is := require.New(t)

	type Inner struct {
		Value string
	}

	type Outer struct {
		Values *[]Inner
	}

	values := []Inner{{Value: "first"}, {Value: "second"}}
	data := Outer{Values: &values}

	value, err := GetI(data, "Values[0].Value")
	is.NoError(err)
	is.Equal(value.String(), "first")
}

func ExampleLookupString() {
	type Cast struct {
		Actor, Role string
	}

	type Movies struct {
		Cast []Cast
	}

	movies := map[string]Movies{
		"Dr. No": {Cast: []Cast{
			{Actor: "Sean Connery", Role: "Bond"},
			{Actor: "Joseph Wiseman", Role: "Dr. No"},
		}},
	}

	q := `["Dr. No"].Cast[0].Actor`
	value, _ := Get(movies, q)
	fmt.Println(q, "->", value.Interface())

	// aggregate Cast slice values by omitting index
	q = `Dr\. No.Cast.Role`
	value, _ = Get(movies, q)
	fmt.Println(q, "->", value.Interface())

	q = `Dr\. No.Cast[0].Actor`
	value, _ = Get(movies, q)
	fmt.Println(q, "->", value.Interface())

	// Output:
	// ["Dr. No"].Cast[0].Actor -> Sean Connery
	// Dr\. No.Cast.Role -> [Bond Dr. No]
	// Dr\. No.Cast[0].Actor -> Sean Connery
}

func ExampleLookup() {
	type ExampleStruct struct {
		Values struct {
			Foo int
		}
	}

	i := ExampleStruct{}
	i.Values.Foo = 10

	value, _ := Lookup(i, "Values", "Foo")
	fmt.Println(value.Interface())
	// Output: 10
}

func ExampleCaseInsensitive() {
	type Cast struct {
		Actor, Role string
	}

	type Movies struct {
		Cast []Cast
	}

	movies := map[string]Movies{
		"Dr. No": {Cast: []Cast{
			{Actor: "Sean Connery", Role: "Bond"},
			{Actor: "Joseph Wiseman", Role: "Dr. No"},
		}},
	}

	q := `dr\. No.Cast.Role`
	value, _ := GetI(movies, q)
	fmt.Println(q, "->", value.Interface())

	q = `Dr\. no.Cast[0].Actor`
	value, _ = GetI(movies, q)
	fmt.Println(q, "->", value.Interface())

	q = `["Dr. No"].Cast[0].actor`
	value, _ = GetI(movies, q)
	fmt.Println(q, "->", value.Interface())

	// Output:
	// dr\. No.Cast.Role -> [Bond Dr. No]
	// Dr\. no.Cast[0].Actor -> Sean Connery
	// ["Dr. No"].Cast[0].actor -> Sean Connery
}

type MyStruct struct {
	String      string
	Map         map[string]int
	Nested      *MyStruct
	StructSlice []*MyStruct
	Interface   interface{}
}

type MyKey string

var mapFixtureNamed = map[MyKey]int{"foo": 42}
var mapFixture = map[string]int{"foo": 42}
var structFixture = MyStruct{
	String:    "foo",
	Map:       mapFixture,
	Interface: "foo",
	StructSlice: []*MyStruct{
		{Map: mapFixture, String: "foo", StructSlice: []*MyStruct{{String: "bar"}, {String: "foo"}}},
		{Map: mapFixture, String: "qux", StructSlice: []*MyStruct{{String: "qux"}, {String: "baz"}}},
	},
}

var mapComplexFixture = map[string]interface{}{
	"map": map[string]interface{}{
		"bar": 1,
	},
	"list": []map[string]interface{}{
		{"baz": 1},
		{"baz": 2},
		{"baz": 3},
	},
	"nested": map[string]interface{}{
		"fav.color": "blue",
	},
}

var caseFixtureStruct = struct {
	Foo       int
	TestField int
	Testfield int
	testField int
}{
	0, 1, 2, 3,
}

var caseFixtureMap = map[string]int{
	"Foo":     0,
	"TestKey": 1,
	"Testkey": 2,
	"testKey": 3,
}

var intSliceFixture = []interface{}{1, 10, []interface{}{100}}
