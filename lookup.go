/*
Small library on top of reflect to lookup values in Structs or Maps. Using a
very simple DSL you can access to any property.
*/
package objpath

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

const (
	SplitToken     = "."
	IndexCloseChar = "]"
	IndexOpenChar  = "["
)

var (
	ErrMalformedIndex     = errors.New("malformed index key")
	ErrInvalidIndexUsage  = errors.New("invalid index key usage")
	ErrKeyNotFound        = errors.New("unable to find the key")
	ErrMalformedKeyQuotes = errors.New("malformedKeyQuotes")
	ErrMalformedSubPath   = errors.New("malformed subpath")
)

func getKeyIndex(expr string, isFirst bool, quoteChar string) ([]interface{}, error) {
	if expr == "" {
		return nil, fmt.Errorf("empty expr")
	}

	var res []interface{}

	parts := splitToken(expr, "[", quoteChar)
	// key
	if len(parts) == 1 {
		key := parts[0]
		if key == "" {
			return nil, ErrMalformedSubPath
		}
		res = append(res, parts[0])
		return res, nil
	}

	if parts[0] == "" && !isFirst {
		return nil, fmt.Errorf("indexed expression must have parent")
	}

	// "[0]" has no parent
	if parts[0] != "" {
		res = append(res, parts[0])
	}

	// key[sub][sub2] => key, sub], sub2]
	for _, sub := range parts[1:] {
		subParts := splitToken(sub, "]", quoteChar)
		if len(subParts) != 2 {
			return nil, ErrMalformedSubPath
		}

		index := subParts[0]
		// quoted ["sub"]
		if strings.HasPrefix(index, quoteChar) {
			index = strings.Trim(index, quoteChar)
			res = append(res, index)
			continue
		}

		// must be an integer if not quoted
		n, err := strconv.Atoi(index)
		if err != nil {
			return nil, ErrMalformedSubPath
		}
		res = append(res, n)
	}
	return res, nil

}

// SplitPath converts an object string path to a slice of keys and indices.
// eg `key["sub.key"][0]` => []any{"key", "sub.key", 0}
func SplitPath(path string, quoteChar string) ([]interface{}, error) {
	parts := splitToken(path, ".", quoteChar)
	var res []interface{}

	for i, part := range parts {
		keys, err := getKeyIndex(part, i == 0, quoteChar)
		if err != nil {
			return nil, fmt.Errorf("subpath %s: %w", part, err)
		}

		res = append(res, keys...)
	}

	//fmt.Printf("%+v\n", res)
	return res, nil
}

// Get performs a lookup into a value, using a string object path. String indices
// may be quoted with double quotes.
func Get(i interface{}, path string) (reflect.Value, error) {
	return GetQ(i, path, `"`)
}

// Get performs a lookup into a value, using a string object path. String indices
// may be quoted with caller defined `quoteChar`.
func GetQ(i interface{}, path string, quoteChar string) (reflect.Value, error) {
	paths, err := SplitPath(path, quoteChar)
	if err != nil {
		return reflect.Value{}, err
	}
	return Lookup(i, paths...)
}

// GetI is the same as Get, but the path is not case sensitive.
func GetI(i interface{}, path string) (reflect.Value, error) {
	return GetIQ(i, path, `"`)
}

// GetIQ is the same as GetI, but the path is not case sensitive.
func GetIQ(i interface{}, path string, quoteChar string) (reflect.Value, error) {
	paths, err := SplitPath(path, quoteChar)
	if err != nil {
		return reflect.Value{}, err
	}
	return LookupI(i, paths...)
}

// Lookup performs a lookup into a value, using a path of keys. The key should
// match with a Field or a MapIndex. For slice you can use the syntax key[index]
// to access a specific index. If one key owns to a slice and an index is not
// specified the rest of the path will be applied to evaly value of the
// slice, and the value will be merged into a slice.
func Lookup(i interface{}, path ...interface{}) (reflect.Value, error) {
	return lookup(i, false, path...)
}

// LookupI is the same as Lookup, but the path keys are not case sensitive.
func LookupI(i interface{}, path ...interface{}) (reflect.Value, error) {
	return lookup(i, true, path...)
}

func lookup(i interface{}, caseInsensitive bool, path ...interface{}) (reflect.Value, error) {
	value := reflect.ValueOf(i)
	var err error
	var parent reflect.Value

	// top := value.Interface()
	// fmt.Printf("%+v\n", top)

	for j, part := range path {
		parent = value

		switch v := part.(type) {
		default:
			return reflect.Value{}, ErrMalformedSubPath
		case int:
			value, err = getValueByIndex(value, v)
			if err == nil {
				continue
			}
		case string:
			value, err = getValueByName(value, v, caseInsensitive)
			if err == nil {
				continue
			}
		}

		// if no
		if !isAggregable(parent) {
			break
		}

		value, err = aggregateAggregableValue(parent, caseInsensitive, path[j:])
		break

		// x := value.Interface()
		// fmt.Printf("%+v\n", x)

	}

	return value, err
}

func getValueByIndex(value reflect.Value, index int) (reflect.Value, error) {
	if !value.IsValid() {
		return reflect.Value{}, ErrKeyNotFound
	}

	if index < 0 {
		return reflect.Value{}, ErrInvalidIndexUsage
	}

	if value.Kind() == reflect.Ptr {
		value = value.Elem()
	}

	if value.Type().Kind() != reflect.Slice {
		return reflect.Value{}, ErrInvalidIndexUsage
	}

	value = value.Index(index)

	if value.Kind() == reflect.Ptr || value.Kind() == reflect.Interface {
		value = value.Elem()
	}

	return value, nil
}

func getValueByName(v reflect.Value, key string, caseInsensitive bool) (reflect.Value, error) {
	var value reflect.Value

	switch v.Kind() {
	case reflect.Ptr, reflect.Interface:
		return getValueByName(v.Elem(), key, caseInsensitive)
	case reflect.Struct:
		value = v.FieldByName(key)

		if caseInsensitive && value.Kind() == reflect.Invalid {
			// We don't use FieldByNameFunc, since it returns zero value if the
			// match func matches multiple fields. Iterate here and return the
			// first matching field.
			for i := 0; i < v.NumField(); i++ {
				if strings.EqualFold(v.Type().Field(i).Name, key) {
					value = v.Field(i)
					break
				}
			}
		}

	case reflect.Map:
		kValue := reflect.Indirect(reflect.New(v.Type().Key()))
		kValue.SetString(key)
		value = v.MapIndex(kValue)
		if caseInsensitive && value.Kind() == reflect.Invalid {
			iter := v.MapRange()
			for iter.Next() {
				if strings.EqualFold(key, iter.Key().String()) {
					kValue.SetString(iter.Key().String())
					value = v.MapIndex(kValue)
					break
				}
			}
		}
	}

	if !value.IsValid() {
		return reflect.Value{}, ErrKeyNotFound
	}

	if value.Kind() == reflect.Ptr || value.Kind() == reflect.Interface {
		value = value.Elem()
	}

	return value, nil
}

func aggregateAggregableValue(v reflect.Value, caseInsensitive bool, path []interface{}) (reflect.Value, error) {
	values := make([]reflect.Value, 0)

	l := v.Len()
	if l == 0 {
		ty, ok := lookupType(v.Type(), path...)
		if !ok {
			return reflect.Value{}, ErrKeyNotFound
		}
		return reflect.MakeSlice(reflect.SliceOf(ty), 0, 0), nil
	}

	index := indexFunction(v)
	for i := 0; i < l; i++ {
		var value reflect.Value
		var err error

		if caseInsensitive {
			value, err = LookupI(index(i).Interface(), path...)
		} else {
			value, err = Lookup(index(i).Interface(), path...)
		}
		if err != nil {
			return reflect.Value{}, err
		}

		values = append(values, value)
	}

	return mergeValue(values), nil
}

func indexFunction(v reflect.Value) func(i int) reflect.Value {
	switch v.Kind() {
	case reflect.Slice:
		return v.Index
	case reflect.Map:
		keys := v.MapKeys()
		return func(i int) reflect.Value {
			return v.MapIndex(keys[i])
		}
	default:
		panic("unsupported kind for index")
	}
}

func mergeValue(values []reflect.Value) reflect.Value {
	values = removeZeroValues(values)
	l := len(values)
	if l == 0 {
		return reflect.Value{}
	}

	sample := values[0]
	mergeable := isMergeable(sample)

	t := sample.Type()
	if mergeable {
		t = t.Elem()
	}

	value := reflect.MakeSlice(reflect.SliceOf(t), 0, 0)
	for i := 0; i < l; i++ {
		if !values[i].IsValid() {
			continue
		}

		if mergeable {
			value = reflect.AppendSlice(value, values[i])
		} else {
			value = reflect.Append(value, values[i])
		}
	}

	return value
}

func removeZeroValues(values []reflect.Value) []reflect.Value {
	l := len(values)

	var v []reflect.Value
	for i := 0; i < l; i++ {
		if values[i].IsValid() {
			v = append(v, values[i])
		}
	}

	return v
}

func isAggregable(v reflect.Value) bool {
	k := v.Kind()

	return k == reflect.Map || k == reflect.Slice
}

func isMergeable(v reflect.Value) bool {
	k := v.Kind()
	return k == reflect.Map || k == reflect.Slice
}

func lookupType(ty reflect.Type, path ...interface{}) (reflect.Type, bool) {
	if len(path) == 0 {
		return ty, false
	}

	switch ty.Kind() {
	case reflect.Slice, reflect.Array:
		if _, ok := path[0].(int); ok {
			return lookupType(ty.Elem(), path[1:]...)
		}
		// aggregate
		return lookupType(ty.Elem(), path...)

	case reflect.Map:
		if _, ok := path[0].(string); ok {
			return lookupType(ty.Elem(), path[1:]...)
		}
		// aggregate
		return lookupType(ty.Elem(), path...)
	case reflect.Ptr:
		return lookupType(ty.Elem(), path...)
	case reflect.Interface:
		// We can't know from here without a value. Let's just return this type.
		return ty, true
	case reflect.Struct:
		if key, ok := path[0].(string); ok {
			f, ok := ty.FieldByName(key)
			if ok {
				return lookupType(f.Type, path[1:]...)
			}
		}

	}
	return nil, false
}
