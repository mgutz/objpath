package objpath

import "strings"

// splitToken splits `str` by `token` unless escaped or within quoted strings.
func splitToken(str string, token string, quoteChar string) []string {
	var parts []string
	var currentPart strings.Builder
	isInQuotes := false

	strL := len(str)
	for i := -1; i < strL-1; {
		i++
		ch := str[i : i+1]

		if !isInQuotes && ch == token {
			parts = append(parts, currentPart.String())
			currentPart.Reset()
		} else if !isInQuotes && ch == "\\" && safeSub(str, i+1, 1) == token {
			currentPart.WriteString(token)
			i++
		} else if isInQuotes && ch == quoteChar && safeSub(str, i+1, 1) == quoteChar {
			currentPart.WriteString(ch)
			currentPart.WriteString(ch)
			i++
		} else {
			currentPart.WriteString(ch)
			if ch == quoteChar {
				isInQuotes = !isInQuotes
			}
		}
	}

	return append(parts, currentPart.String())
}

// safeSub extracts a substring from `s` within bounds.
func safeSub(s string, start int, length int) string {
	if start >= 0 && start+length <= len(s) {
		return s[start : start+length]
	}
	return ""
}
